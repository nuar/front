import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TAG_APP_INNERComponent } from './TAG_APP.component';

const routes: Routes = [
  {
    path: '', pathMatch: 'full', component: TAG_APP_INNERComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TAG_APP_INNERRoutingModule { }
