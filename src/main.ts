import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { TAG_APP_INNERModule } from './app/TAG_APP.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(TAG_APP_INNERModule)
  .catch(err => console.log(err));
